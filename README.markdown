# Tensorflow.js Boilerplate

A Pen created on CodePen.io. Original URL: [https://codepen.io/sms82ar/pen/ZEWwYWO](https://codepen.io/sms82ar/pen/ZEWwYWO).

The hello world for TensorFlow.js :-) Absolute minimum needed to import into your website and simply prints the loaded TensorFlow.js version. From  here we can do great things. Clone this to make your own TensorFlow.js powered projects or if you are following a tutorial that needs TensorFlow.js to work.
